import React from "react"
import Layout from "./layout"

export default ({ children, pageContext }) => (
  <Layout>
    {children}
  </Layout>
)
